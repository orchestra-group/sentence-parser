from pycorenlp.corenlp import StanfordCoreNLP
from queue import Queue
import json
import os
import argparse


def processing_data(data):
    """
    A function that organizes the json file when the heuristic starts from the parent and down to child in the order in which they are inserted.
    :param data: the basic parse of dependencies tree
    :return: the dependencies tree order and without punct and connection word
    """
    new_data = []
    q = Queue(maxsize=len(data))
    q.put(data[0])
    while not q.empty():
        node_from_queue = q.get()
        new_data.append(node_from_queue)
        for node in data:
            if node_from_queue['dependentGloss'] == node['governorGloss']:
                if node['dep'] != 'case' and node['dep'] != "punct":
                    q.put(node)

    return new_data


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--text_file', default="None", help='inset the input text file')
    args = parser.parse_args()
    if not os.path.isfile(args.text_file):
        print("file dont exists")
        exit(1)
    host = "http://localhost"
    port = "9000"
    nlp = StanfordCoreNLP(host + ":" + port)
    file_reader = open(args.text_file, 'r+')
    sentenses = file_reader.read()
    output = nlp.annotate(
        sentenses,
        properties={
            "outputFormat": "json",
            "annotators": "depparse,entitymentions,sentiment,tokenize,ssplit,parse,lemma,ner"
        }
    )
    for i in range(len(output['sentences'])):
        with open('sentence_num_'+str(i)+'.json', 'w+') as file:
            data = processing_data(output["sentences"][i]['basicDependencies'])
            json.dump(data, file, indent=4)
            file.close()
    file_reader.close()
