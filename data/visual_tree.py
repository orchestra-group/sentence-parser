import json
from graphviz import Digraph
import subprocess


def create_tree(graph_data, filename):
    """
    functions that create the tree graph of sentence
    :param graph_data: the json data
    :param filename: name data of export
    :return: dot object that visualize the data
    """
    dict_nodes = {}
    dot = Digraph(comment='Tree graph', filename=filename)
    edge_later = []
    for i in range(len(graph_data)):
        node = graph_data[i]
        dict_nodes[node['dependentGloss']] = str(i)
        dot.node(str(i), node['dependentGloss'])
        if node['governor'] > 0:
            if node['governorGloss'] in dict_nodes.keys():
                dot.edge(dict_nodes[node['governorGloss']], dict_nodes[node['dependentGloss']], label=node['dep'])
            else:
                edge_later.append((node['governorGloss'], node['dependentGloss'], node['dep']))

    for head, tail, desc in edge_later:
        dot.edge(dict_nodes[tail], dict_nodes[head], desc)
    return dot


if __name__ == "__main__":
    res = subprocess.Popen('find . -name "sentence_num_*.json"', stdout=subprocess.PIPE, shell=True).stdout.readlines()
    files = list(map(lambda x: str(x).lstrip('b').replace('\\n', "").replace('\'',""), res))
    for i in range(len(files)):
        file = open(files[i].lstrip('./'), 'r+')
        graph_data = json.load(file)
        tree = create_tree(graph_data, 'graph_sentence_' + str(i) + '.gv')
        tree.render()
