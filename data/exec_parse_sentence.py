import os
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--text_file', default="None", help='inset the input text file')
    args = parser.parse_args()
    try:
        os.system('python3 parser_sentence.py --text_file ' + args.text_file)
    except Exception as e:
        print(e)
        exit(1)
    os.system('python3 visual_tree.py')
