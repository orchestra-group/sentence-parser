FROM nlpbox/corenlp
RUN apk add --update --no-cache python3 graphviz ttf-freefont && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip && pip3 install --no-cache --upgrade pip setuptools 
RUN pip3 install  graphviz
RUN pip3 install pycorenlp

