# Running Contianer #

Build an docker container using command

```bash
docker build -t stadford-nlp-core .
```

create your file text that contain the english text in data folder .

to run the contianer use :

```bash
docker run --name standford-nlp-core -p 9000:9000 -v `pwd`/data:/data -it stadford-nlp-core  /bin/sh  
```

### Run NLP core ###

to run proccess of NLP in the directory /opt/corenlp (you are in where you log to container) :

```bash
java -mx3g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer -port 9000 &
```

### Run anaylze sentence ###

go to data folder :

```bash
cd /data/
```

run the script with follow command :

```bash
python3 exec_parse_sentence.py --text_file <name_file> 
```

in the data folder will be create files of json, gv and pdf of each sentence .

the json file is description of depndecies on sentences .

the gv file is description on format graphiz (dot object) of depndecies on sentences .

the pdf file is visual of tree dependecies .


